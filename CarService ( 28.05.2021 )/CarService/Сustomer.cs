﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CarService
{
    public class DataBaseOfСustomers
    {
        public static string PathToFolder = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\")) + @"DataBase\";
        public List<Сustomer> OurCustomers = new List<Сustomer>();
        public string PathToData { get; private set; }
        public DataBaseOfСustomers(string DataName)
        {
            PathToData = PathToFolder + DataName + ".txt";
            DirectoryInfo dirInfo = new DirectoryInfo(PathToData);
            if (dirInfo.Exists)
            {
                File.Delete(PathToData);
                using (FileStream fstream = new FileStream(PathToData, FileMode.OpenOrCreate))
                {
                }
            }
        }


    }
    public class Сustomer : IComparable
    {
        private string fullname, contactnumber, carnumber, reason;
        public DateTime ApplicationTime { get; set; }
        public string FullName
        {
            get { return fullname; }
            set
            {
                fullname = (value != null || value != "") ? value : "Клиент без имени!";
            }
        }
        public string СontactNumber
        {
            get { return contactnumber; }
            set
            {
                contactnumber = (value != null || value != "") ? value : "Клиент без телефона!";
            }
        }
        public string СarNumber
        {
            get { return carnumber; }
            set
            {
                carnumber = (value != null || value != "") ? value : "Машина не на учете!";
            }
        }
        public string Reason
        {
            get { return reason; }
            set
            {
                reason = (value != null || value != "") ? value : "Полное ТО!!!!";
            }
        }
        public string Status { get; set; }
        public Сustomer(string pib, string contactN, string CarN, string reasont, string status)
        {
            FullName = pib;
            СontactNumber = contactN;
            СarNumber = CarN;
            Reason = reasont;
            Status = status;
            ApplicationTime = DateTime.Now;
        }
        public Сustomer(string pib, string contactN, string CarN, string reasont, string status, DateTime TimeOf) : this(pib, contactN, CarN, reasont, status)
        {
            ApplicationTime = TimeOf;
        }

        public Сustomer()
        {
        }

        public override string ToString()
        {
            return $"<{FullName}><{СontactNumber}><{СarNumber}><{Reason}><{Status}><{ApplicationTime.ToShortDateString()}>";
        }
        public int CompareTo(object obj)
        {
            Сustomer p = obj as Сustomer;
            if (p != null)
            {
                if (ApplicationTime < p.ApplicationTime)
                {
                    return 1;
                }
                else if (ApplicationTime == p.ApplicationTime)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                throw new Exception("Невозможно сравнить два объекта");
            }
        }
    }
}


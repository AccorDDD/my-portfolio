﻿
namespace CarService
{
    partial class OurProgramm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OurProgramm));
            this.SearchBox = new System.Windows.Forms.GroupBox();
            this.BackAll = new System.Windows.Forms.Button();
            this.FindButt = new System.Windows.Forms.Button();
            this.StatusBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DateBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ReasonBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CarNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PIBbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TelephoneBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OurCustomersBase = new System.Windows.Forms.DataGridView();
            this.ButtonsForDo = new System.Windows.Forms.ToolStrip();
            this.AddButton = new System.Windows.Forms.ToolStripButton();
            this.EditButton = new System.Windows.Forms.ToolStripButton();
            this.DeleteButton = new System.Windows.Forms.ToolStripButton();
            this.ClearAllButton = new System.Windows.Forms.ToolStripButton();
            this.ExitButton = new System.Windows.Forms.ToolStripButton();
            this.SaveAsButton = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SearchBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OurCustomersBase)).BeginInit();
            this.ButtonsForDo.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchBox
            // 
            this.SearchBox.Controls.Add(this.BackAll);
            this.SearchBox.Controls.Add(this.FindButt);
            this.SearchBox.Controls.Add(this.StatusBox);
            this.SearchBox.Controls.Add(this.label6);
            this.SearchBox.Controls.Add(this.DateBox);
            this.SearchBox.Controls.Add(this.label5);
            this.SearchBox.Controls.Add(this.ReasonBox);
            this.SearchBox.Controls.Add(this.label4);
            this.SearchBox.Controls.Add(this.CarNumber);
            this.SearchBox.Controls.Add(this.label3);
            this.SearchBox.Controls.Add(this.PIBbox);
            this.SearchBox.Controls.Add(this.label2);
            this.SearchBox.Controls.Add(this.TelephoneBox);
            this.SearchBox.Controls.Add(this.label1);
            this.SearchBox.Location = new System.Drawing.Point(906, 177);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SearchBox.Size = new System.Drawing.Size(273, 345);
            this.SearchBox.TabIndex = 0;
            this.SearchBox.TabStop = false;
            this.SearchBox.Text = "ПОИСК";
            // 
            // BackAll
            // 
            this.BackAll.Location = new System.Drawing.Point(125, 301);
            this.BackAll.Name = "BackAll";
            this.BackAll.Size = new System.Drawing.Size(142, 23);
            this.BackAll.TabIndex = 14;
            this.BackAll.Text = "Вывести всю базу";
            this.BackAll.UseVisualStyleBackColor = true;
            this.BackAll.Click += new System.EventHandler(this.BackAll_Click);
            // 
            // FindButt
            // 
            this.FindButt.Location = new System.Drawing.Point(125, 252);
            this.FindButt.Name = "FindButt";
            this.FindButt.Size = new System.Drawing.Size(142, 43);
            this.FindButt.TabIndex = 13;
            this.FindButt.Text = "Найти";
            this.FindButt.UseVisualStyleBackColor = true;
            this.FindButt.Click += new System.EventHandler(this.FindButt_Click);
            // 
            // StatusBox
            // 
            this.StatusBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.StatusBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StatusBox.FormattingEnabled = true;
            this.StatusBox.Items.AddRange(new object[] {
            "All",
            "Обрабатывается",
            "InProgress",
            "Закрыта"});
            this.StatusBox.Location = new System.Drawing.Point(125, 214);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(142, 23);
            this.StatusBox.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Статус:";
            // 
            // DateBox
            // 
            this.DateBox.Location = new System.Drawing.Point(125, 175);
            this.DateBox.Name = "DateBox";
            this.DateBox.Size = new System.Drawing.Size(142, 23);
            this.DateBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Дата Заявки:";
            // 
            // ReasonBox
            // 
            this.ReasonBox.Location = new System.Drawing.Point(125, 137);
            this.ReasonBox.Name = "ReasonBox";
            this.ReasonBox.Size = new System.Drawing.Size(142, 23);
            this.ReasonBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Причина поломки:";
            // 
            // CarNumber
            // 
            this.CarNumber.Location = new System.Drawing.Point(125, 97);
            this.CarNumber.Name = "CarNumber";
            this.CarNumber.Size = new System.Drawing.Size(142, 23);
            this.CarNumber.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Номер машины:";
            // 
            // PIBbox
            // 
            this.PIBbox.Location = new System.Drawing.Point(125, 28);
            this.PIBbox.Name = "PIBbox";
            this.PIBbox.Size = new System.Drawing.Size(142, 23);
            this.PIBbox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Телефон:";
            // 
            // TelephoneBox
            // 
            this.TelephoneBox.Location = new System.Drawing.Point(125, 61);
            this.TelephoneBox.Name = "TelephoneBox";
            this.TelephoneBox.Size = new System.Drawing.Size(142, 23);
            this.TelephoneBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ф.И.О:";
            // 
            // OurCustomersBase
            // 
            this.OurCustomersBase.AllowUserToAddRows = false;
            this.OurCustomersBase.AllowUserToDeleteRows = false;
            this.OurCustomersBase.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.OurCustomersBase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.OurCustomersBase.Location = new System.Drawing.Point(12, 12);
            this.OurCustomersBase.Name = "OurCustomersBase";
            this.OurCustomersBase.ReadOnly = true;
            this.OurCustomersBase.RowTemplate.Height = 25;
            this.OurCustomersBase.Size = new System.Drawing.Size(888, 529);
            this.OurCustomersBase.TabIndex = 1;
            // 
            // ButtonsForDo
            // 
            this.ButtonsForDo.Dock = System.Windows.Forms.DockStyle.None;
            this.ButtonsForDo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddButton,
            this.EditButton,
            this.DeleteButton,
            this.ClearAllButton,
            this.ExitButton,
            this.SaveAsButton});
            this.ButtonsForDo.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.ButtonsForDo.Location = new System.Drawing.Point(903, 12);
            this.ButtonsForDo.Name = "ButtonsForDo";
            this.ButtonsForDo.Size = new System.Drawing.Size(24, 149);
            this.ButtonsForDo.TabIndex = 2;
            this.ButtonsForDo.Text = "toolStrip1";
            this.ButtonsForDo.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ButtonsForDo_ItemClicked);
            // 
            // AddButton
            // 
            this.AddButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.AddButton.DoubleClickEnabled = true;
            this.AddButton.Image = global::CarService.Properties.Resources.Add;
            this.AddButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(22, 20);
            this.AddButton.Text = "Add New User";
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.EditButton.Image = global::CarService.Properties.Resources.Edit;
            this.EditButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(22, 20);
            this.EditButton.Text = "Edit Selected User";
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.DeleteButton.Image = global::CarService.Properties.Resources.Delete;
            this.DeleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(22, 20);
            this.DeleteButton.Text = "Deletе Selected User";
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // ClearAllButton
            // 
            this.ClearAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ClearAllButton.Image = global::CarService.Properties.Resources.Clear;
            this.ClearAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ClearAllButton.Name = "ClearAllButton";
            this.ClearAllButton.Size = new System.Drawing.Size(22, 20);
            this.ClearAllButton.Text = "Clear All Data";
            this.ClearAllButton.Click += new System.EventHandler(this.ClearAllButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ExitButton.Image = global::CarService.Properties.Resources.Exit;
            this.ExitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(22, 20);
            this.ExitButton.Text = "Exit";
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // SaveAsButton
            // 
            this.SaveAsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.SaveAsButton.Image = global::CarService.Properties.Resources.Save;
            this.SaveAsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveAsButton.Name = "SaveAsButton";
            this.SaveAsButton.Size = new System.Drawing.Size(22, 20);
            this.SaveAsButton.Text = "Save As";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // OurProgramm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1191, 553);
            this.Controls.Add(this.ButtonsForDo);
            this.Controls.Add(this.OurCustomersBase);
            this.Controls.Add(this.SearchBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OurProgramm";
            this.Text = "DataBase";
            this.SearchBox.ResumeLayout(false);
            this.SearchBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OurCustomersBase)).EndInit();
            this.ButtonsForDo.ResumeLayout(false);
            this.ButtonsForDo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox SearchBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView OurCustomersBase;
        private System.Windows.Forms.TextBox ReasonBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CarNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PIBbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TelephoneBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox DateBox;
        private System.Windows.Forms.ComboBox StatusBox;
        private System.Windows.Forms.Button BackAll;
        private System.Windows.Forms.Button FindButt;
        private System.Windows.Forms.ToolStrip ButtonsForDo;
        private System.Windows.Forms.ToolStripButton AddButton;
        private System.Windows.Forms.ToolStripButton EditButton;
        private System.Windows.Forms.ToolStripButton DeleteButton;
        private System.Windows.Forms.ToolStripButton ClearAllButton;
        private System.Windows.Forms.ToolStripButton ExitButton;
        private System.Windows.Forms.ToolStripButton SaveAsButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}
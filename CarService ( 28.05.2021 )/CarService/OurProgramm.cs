﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace CarService
{
    public partial class OurProgramm : Form
    {
        DataBaseOfСustomers Customers;
        public OurProgramm()
        {
            InitializeComponent();
            Customers = new DataBaseOfСustomers("DataBase");
            OurCustomersBase.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "FIO";
            column.Name = "Ф.И.О";
            column.Width = 190;
            OurCustomersBase.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Telephone";
            column.Name = "Телефон";
            OurCustomersBase.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "CarNomer";
            column.Name = "Номер машины";
            OurCustomersBase.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "ReasonOfMistake";
            column.Name = "Причина";
            column.Width = 200;
            OurCustomersBase.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Status";
            column.Name = "Статус";
            column.Width = 110;
            OurCustomersBase.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Data";
            column.Name = "Дата занесения в базу";
            column.Width = 70;
            OurCustomersBase.Columns.Add(column);

            int check = 0;
            using (StreamReader test = new StreamReader(Customers.PathToData, Encoding.UTF8))
            {
                string s;
                while ((s = test.ReadLine()) != null)
                {
                    check++;
                }
            }
            using (StreamReader sr = new StreamReader(Customers.PathToData, Encoding.UTF8))
            {
                string s;
                try
                {
                    Customers.OurCustomers = new List<Сustomer>(check);
                    int i = 0;
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('<', '>');
                        Сustomer newLcustomer = new Сustomer(split[1], split[3], split[5], split[7], split[9], DateTime.Parse(split[11]));
                        OurCustomersBase.Rows.Add(newLcustomer);
                        Customers.OurCustomers.Add(new Сustomer(newLcustomer.FullName, newLcustomer.СontactNumber, newLcustomer.СarNumber, newLcustomer.Reason, newLcustomer.Status, newLcustomer.ApplicationTime));
                        i++;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Произошла ошибка: \n{ex.Message}" + MessageBoxButtons.OK + MessageBoxIcon.Error);
                }
                finally
                {
                    Customers.OurCustomers.Sort();
                    for (int i = 0; i < OurCustomersBase.RowCount; i++)
                    {
                        OurCustomersBase.Rows[i].Cells[0].Value = Customers.OurCustomers[i].FullName;
                        OurCustomersBase.Rows[i].Cells[1].Value = Customers.OurCustomers[i].СontactNumber;
                        OurCustomersBase.Rows[i].Cells[2].Value = Customers.OurCustomers[i].СarNumber;
                        OurCustomersBase.Rows[i].Cells[3].Value = Customers.OurCustomers[i].Reason;
                        OurCustomersBase.Rows[i].Cells[4].Value = Customers.OurCustomers[i].Status;
                        OurCustomersBase.Rows[i].Cells[5].Value = Customers.OurCustomers[i].ApplicationTime.ToShortDateString();
                    }
                    OurCustomersBase.Refresh();
                }
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddOrEditMenu Add = new AddOrEditMenu();
            if (Add.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(Customers.PathToData, true))
                {
                    sw.WriteLine(Add.GetCustomer());
                }
                Customers.OurCustomers.Add(Add.GetCustomer());
                OurCustomersBase.Rows.Add(Customers.OurCustomers[Customers.OurCustomers.Count - 1]);
                Customers.OurCustomers.Sort();
                for (int i = 0; i < OurCustomersBase.RowCount; i++)
                {
                    OurCustomersBase.Rows[i].Cells[0].Value = Customers.OurCustomers[i].FullName;
                    OurCustomersBase.Rows[i].Cells[1].Value = Customers.OurCustomers[i].СontactNumber;
                    OurCustomersBase.Rows[i].Cells[2].Value = Customers.OurCustomers[i].СarNumber;
                    OurCustomersBase.Rows[i].Cells[3].Value = Customers.OurCustomers[i].Reason;
                    OurCustomersBase.Rows[i].Cells[4].Value = Customers.OurCustomers[i].Status;
                    OurCustomersBase.Rows[i].Cells[5].Value = Customers.OurCustomers[i].ApplicationTime.ToShortDateString();
                }
                OurCustomersBase.Refresh();
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            int edit = OurCustomersBase.SelectedCells[0].RowIndex;
            AddOrEditMenu Add = new AddOrEditMenu(Customers.OurCustomers[edit]);
            if (Add.ShowDialog() == DialogResult.OK)
            {
                Customers.OurCustomers[edit] = Add.GetCustomer();
                Customers.OurCustomers.Sort();
                for (int i = 0; i < OurCustomersBase.RowCount; i++)
                {
                    OurCustomersBase.Rows[i].Cells[0].Value = Customers.OurCustomers[i].FullName;
                    OurCustomersBase.Rows[i].Cells[1].Value = Customers.OurCustomers[i].СontactNumber;
                    OurCustomersBase.Rows[i].Cells[2].Value = Customers.OurCustomers[i].СarNumber;
                    OurCustomersBase.Rows[i].Cells[3].Value = Customers.OurCustomers[i].Reason;
                    OurCustomersBase.Rows[i].Cells[4].Value = Customers.OurCustomers[i].Status;
                    OurCustomersBase.Rows[i].Cells[5].Value = Customers.OurCustomers[i].ApplicationTime.ToShortDateString();
                }
                OurCustomersBase.Refresh();
                DirectoryInfo dirInfo = new DirectoryInfo(Customers.PathToData);
                if (!dirInfo.Exists)
                {
                    File.Delete(Customers.PathToData);
                }
                using (FileStream fstream = new FileStream(Customers.PathToData, FileMode.OpenOrCreate))
                {
                }
                using (StreamWriter sw = new StreamWriter(Customers.PathToData, true))
                {
                    for (int i = 0; i < Customers.OurCustomers.Count; i++)
                    {
                        sw.WriteLine(Customers.OurCustomers[i]);
                    }
                }
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            int delet = 0;
            if (MessageBox.Show("Вы хотите удалить выбранные данные?", "Удаление данных", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                delet = OurCustomersBase.SelectedCells[0].RowIndex;
                OurCustomersBase.Rows.RemoveAt(delet);
            }
            Customers.OurCustomers.RemoveAt(delet);
            DirectoryInfo dirInfo = new DirectoryInfo(Customers.PathToData);
            if (!dirInfo.Exists)
            {
                File.Delete(Customers.PathToData);
            }
            using (FileStream fstream = new FileStream(Customers.PathToData, FileMode.OpenOrCreate))
            {
            }
            using (StreamWriter sw = new StreamWriter(Customers.PathToData, true))
            {
                for (int i = 0; i < Customers.OurCustomers.Count; i++)
                {
                    sw.WriteLine(Customers.OurCustomers[i]);
                }
            }
        }

        private void ClearAllButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистить всю таблицу?\n\nВсе данные будут потеряны", "Очистка всех данных", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Customers.PathToData);
                if (!dirInfo.Exists)
                {
                    File.Delete(Customers.PathToData);
                }
                using (FileStream fstream = new FileStream(Customers.PathToData, FileMode.OpenOrCreate))
                {
                }
                Customers.OurCustomers = new List<Сustomer>(0);
                OurCustomersBase.Rows.Clear();
                OurCustomersBase.Refresh();
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрыть приложение?", "Выйти из приложения", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }


        private void ButtonsForDo_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void FindButt_Click(object sender, EventArgs e)
        {
            int checkPr = 0;
            List<Сustomer> OurSearchedCustomers = new List<Сustomer>();
            for (int i = 0; i < Customers.OurCustomers.Count; i++) 
            {
                OurSearchedCustomers.Add(new Сustomer());
                if (!string.IsNullOrEmpty(PIBbox.Text))
                {
                    if(i == 0)
                    {
                        checkPr++;
                    }            
                    if (Customers.OurCustomers[i].FullName == PIBbox.Text)
                    {
                        OurSearchedCustomers[i].FullName = Customers.OurCustomers[i].FullName;                   
                    }
                }
                if (!string.IsNullOrEmpty(TelephoneBox.Text))
                {
                    if (i == 0)
                    {
                        checkPr++;
                    }
                    if (Customers.OurCustomers[i].СontactNumber == TelephoneBox.Text)
                    {
                        OurSearchedCustomers[i].СontactNumber = Customers.OurCustomers[i].СontactNumber;
                    }
                }
                if (!string.IsNullOrEmpty(CarNumber.Text))
                {
                    if (i == 0)
                    {
                        checkPr++;
                    }
                    if (Customers.OurCustomers[i].СarNumber == CarNumber.Text)
                    {
                        OurSearchedCustomers[i].СarNumber = Customers.OurCustomers[i].СarNumber;
                    }
                }
                if (!string.IsNullOrEmpty(ReasonBox.Text))
                {
                    if (i == 0)
                    {
                        checkPr++;
                    }
                    if (Customers.OurCustomers[i].Reason == ReasonBox.Text)
                    {
                        OurSearchedCustomers[i].Reason = Customers.OurCustomers[i].Reason;
                    }
                }
                if (!string.IsNullOrEmpty(DateBox.Text))
                {
                    if (i == 0)
                    {
                        checkPr++;
                    }
                    if (Customers.OurCustomers[i].ApplicationTime.ToShortDateString() == DateBox.Text)
                    {
                        OurSearchedCustomers[i].ApplicationTime = Customers.OurCustomers[i].ApplicationTime;
                    }
                }

                if (!string.IsNullOrEmpty(StatusBox.Text))
                {
                    if(StatusBox.Text == "All")
                    {
                        continue;
                    }
                    if (i == 0)
                    {
                        checkPr++;
                    }
                    if (Customers.OurCustomers[i].Status == StatusBox.Text)
                    {
                        OurSearchedCustomers[i].Status = Customers.OurCustomers[i].Status;
                    }
                }
            }
            Customers.OurCustomers.Sort();
            DirectoryInfo dirInfo = new DirectoryInfo(Customers.PathToData);
            if (!dirInfo.Exists)
            {
                File.Delete(Customers.PathToData);
            }
            using (FileStream fstream = new FileStream(Customers.PathToData, FileMode.OpenOrCreate))
            {
            }
            using (StreamWriter sw = new StreamWriter(Customers.PathToData, true))
            {
                for (int i = 0; i < Customers.OurCustomers.Count; i++)
                {
                    sw.WriteLine(Customers.OurCustomers[i]);
                }
            }
            using (StreamReader test = new StreamReader(Customers.PathToData, Encoding.UTF8))
            {
                int helpindex = 0;
                OurCustomersBase.Rows.Clear();
                int x = 0;
                string s;
                while ((s = test.ReadLine()) != null)
                {
                    string OurSearchInfo = OurSearchedCustomers[x].ToString();
                    if (OurSearchedCustomers[x].ApplicationTime.ToShortDateString() == "01.01.0001")
                    {
                        OurSearchInfo = OurSearchInfo.Remove(OurSearchInfo.Length - 11);
                    }
                    int checkcount = 0;
                    string[] OurCustomersInStatic = OurSearchInfo.Split('<', '>');
                    helpindex++;
                    for (int i = 0; i < OurCustomersInStatic.Length; i++)
                    {
                        if(OurCustomersInStatic[i] == "")
                        {
                            continue;
                        }
                        if (s.Contains(OurCustomersInStatic[i]))
                        {
                            checkcount++;
                        }
                    }
                    if (checkcount == checkPr)
                    {
                        OurCustomersBase.Rows.Add(Customers.OurCustomers[helpindex - 1]);
                        OurCustomersBase.Rows[OurCustomersBase.RowCount - 1].Cells[0].Value = Customers.OurCustomers[helpindex - 1].FullName;
                        OurCustomersBase.Rows[OurCustomersBase.RowCount - 1].Cells[1].Value = Customers.OurCustomers[helpindex - 1].СontactNumber;
                        OurCustomersBase.Rows[OurCustomersBase.RowCount - 1].Cells[2].Value = Customers.OurCustomers[helpindex - 1].СarNumber;
                        OurCustomersBase.Rows[OurCustomersBase.RowCount - 1].Cells[3].Value = Customers.OurCustomers[helpindex - 1].Reason;
                        OurCustomersBase.Rows[OurCustomersBase.RowCount - 1].Cells[4].Value = Customers.OurCustomers[helpindex - 1].Status;
                        OurCustomersBase.Rows[OurCustomersBase.RowCount - 1].Cells[5].Value = Customers.OurCustomers[helpindex - 1].ApplicationTime.ToShortDateString();
                        if (x == OurSearchedCustomers.Count)
                        {
                            break;
                        }
                    }
                    x++;
                }
                OurCustomersBase.Refresh();
            }
        } 
    
        private void BackAll_Click(object sender, EventArgs e)
        {
            OurCustomersBase.Rows.Clear();
            for (int i = 0; i < Customers.OurCustomers.Count; i++)
            {
                OurCustomersBase.Rows.Add(Customers.OurCustomers[i]);
                OurCustomersBase.Rows[i].Cells[0].Value = Customers.OurCustomers[i].FullName;
                OurCustomersBase.Rows[i].Cells[1].Value = Customers.OurCustomers[i].СontactNumber;
                OurCustomersBase.Rows[i].Cells[2].Value = Customers.OurCustomers[i].СarNumber;
                OurCustomersBase.Rows[i].Cells[3].Value = Customers.OurCustomers[i].Reason;
                OurCustomersBase.Rows[i].Cells[4].Value = Customers.OurCustomers[i].Status;
                OurCustomersBase.Rows[i].Cells[5].Value = Customers.OurCustomers[i].ApplicationTime.ToShortDateString();
            }
            OurCustomersBase.Refresh();
        }
    }
   } 


﻿
namespace CarService
{
    partial class AddOrEditMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddOrEditMenu));
            this.Change = new System.Windows.Forms.GroupBox();
            this.ChangeButton = new System.Windows.Forms.Button();
            this.StatusBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DateBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ReasonBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.CarNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PIBbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TelephoneBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Change.SuspendLayout();
            this.SuspendLayout();
            // 
            // Change
            // 
            this.Change.Controls.Add(this.ChangeButton);
            this.Change.Controls.Add(this.StatusBox);
            this.Change.Controls.Add(this.label6);
            this.Change.Controls.Add(this.DateBox);
            this.Change.Controls.Add(this.label5);
            this.Change.Controls.Add(this.ReasonBox);
            this.Change.Controls.Add(this.label4);
            this.Change.Controls.Add(this.CarNumber);
            this.Change.Controls.Add(this.label3);
            this.Change.Controls.Add(this.PIBbox);
            this.Change.Controls.Add(this.label2);
            this.Change.Controls.Add(this.TelephoneBox);
            this.Change.Controls.Add(this.label1);
            this.Change.Location = new System.Drawing.Point(12, 12);
            this.Change.Name = "Change";
            this.Change.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Change.Size = new System.Drawing.Size(343, 426);
            this.Change.TabIndex = 1;
            this.Change.TabStop = false;
            this.Change.Text = "Добавить/Изменить";
            // 
            // ChangeButton
            // 
            this.ChangeButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChangeButton.Location = new System.Drawing.Point(125, 252);
            this.ChangeButton.Name = "ChangeButton";
            this.ChangeButton.Size = new System.Drawing.Size(142, 43);
            this.ChangeButton.TabIndex = 13;
            this.ChangeButton.Text = "Добавит/Изменить";
            this.ChangeButton.UseVisualStyleBackColor = true;
            this.ChangeButton.Click += new System.EventHandler(this.ChangeButton_Click);
            // 
            // StatusBox
            // 
            this.StatusBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StatusBox.FormattingEnabled = true;
            this.StatusBox.Items.AddRange(new object[] {
            "Обрабатывается",
            "InProgress",
            "Закрыта"});
            this.StatusBox.Location = new System.Drawing.Point(125, 214);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(142, 23);
            this.StatusBox.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 217);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Статус:";
            // 
            // DateBox
            // 
            this.DateBox.Location = new System.Drawing.Point(125, 175);
            this.DateBox.Name = "DateBox";
            this.DateBox.Size = new System.Drawing.Size(142, 23);
            this.DateBox.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Дата Заявки:";
            // 
            // ReasonBox
            // 
            this.ReasonBox.Location = new System.Drawing.Point(125, 137);
            this.ReasonBox.Name = "ReasonBox";
            this.ReasonBox.Size = new System.Drawing.Size(142, 23);
            this.ReasonBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Причина поломки:";
            // 
            // CarNumber
            // 
            this.CarNumber.Location = new System.Drawing.Point(125, 97);
            this.CarNumber.Name = "CarNumber";
            this.CarNumber.Size = new System.Drawing.Size(142, 23);
            this.CarNumber.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Номер машины:";
            // 
            // PIBbox
            // 
            this.PIBbox.Location = new System.Drawing.Point(125, 28);
            this.PIBbox.Name = "PIBbox";
            this.PIBbox.Size = new System.Drawing.Size(142, 23);
            this.PIBbox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Телефон:";
            // 
            // TelephoneBox
            // 
            this.TelephoneBox.Location = new System.Drawing.Point(125, 61);
            this.TelephoneBox.Name = "TelephoneBox";
            this.TelephoneBox.Size = new System.Drawing.Size(142, 23);
            this.TelephoneBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ф.И.О:";
            // 
            // AddOrEditMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 359);
            this.Controls.Add(this.Change);
            this.Cursor = System.Windows.Forms.Cursors.SizeNESW;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddOrEditMenu";
            this.Text = "Edit or Add Menu";
            this.Change.ResumeLayout(false);
            this.Change.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Change;
        private System.Windows.Forms.Button ChangeButton;
        private System.Windows.Forms.ComboBox StatusBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox DateBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ReasonBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CarNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PIBbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TelephoneBox;
        private System.Windows.Forms.Label label1;
    }
}
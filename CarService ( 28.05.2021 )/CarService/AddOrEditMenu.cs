﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CarService
{
    public partial class AddOrEditMenu : Form
    {

        private Сustomer newClient;
        public AddOrEditMenu()
        {
            InitializeComponent();
            newClient = new Сustomer();
            PIBbox.Text = newClient.FullName;
            TelephoneBox.Text = newClient.СontactNumber;
            CarNumber.Text = newClient.СarNumber;
            ReasonBox.Text = newClient.Reason;
            DateBox.Text = DateTime.Now.ToShortDateString();
            StatusBox.Text = newClient.Status;
        }

        public AddOrEditMenu(Сustomer OurPatient)
        {
            InitializeComponent();
            newClient = OurPatient;
            PIBbox.Text = newClient.FullName;
            TelephoneBox.Text = newClient.СontactNumber;
            CarNumber.Text = newClient.СarNumber;
            ReasonBox.Text = newClient.Reason;
            DateBox.Text = newClient.ApplicationTime.ToShortDateString();
            StatusBox.Text = newClient.Status;
        }
        public Сustomer GetCustomer()
        {
            return newClient;
        }

        private void ChangeButton_Click(object sender, EventArgs e)
        {
            newClient.FullName = PIBbox.Text;
            newClient.СontactNumber = TelephoneBox.Text;
            newClient.СarNumber = CarNumber.Text;
            newClient.Reason = ReasonBox.Text;
            newClient.ApplicationTime = DateTime.Parse(DateBox.Text);
            newClient.Status = StatusBox.Text;
            DialogResult = DialogResult.OK;
        }
    }
}

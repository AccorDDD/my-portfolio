﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork8
{
    class Game
    {

    }
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            Game method = new Game();
            string[] words = new string[] { "автомобиль", "лошадь", "окно", "университет", "гилель", "печенье", "отладка", "вертолет", "пранк", "test" };
            char start, repeat, let;
            char[] letters;
            bool win = false;
            int mistake;
            try
            {
                Console.Write(@"Желаете поиграть в 'Поле чудес'? (y/n) ");
                start = Convert.ToChar(Console.ReadLine());
                if (start == 'n')
                {
                    Console.WriteLine("Ваше право...");
                }
                else if (start == 'y')
                {
                    repeat = 'y';
                    while (repeat == 'y')
                    {
                        int count = 0;
                        List<char> mis = new List<char>();
                        win = false;
                        letters = words[rnd.Next(words.Length)].ToCharArray();
                        char[] t = new char[letters.Length];
                        mistake = letters.Length;
                        count = letters.Length;
                        Console.WriteLine($"Ваше слово содержит {letters.Length} букв и вы можете угадывать одну букву за раз");
                        Console.WriteLine($"Вам предоставляется {letters.Length} неправильных угадываний");
                        for (int i = 0; i < letters.Length; i++)
                        {
                            t[i] = '_';
                        }
                        while (win == false & mistake > 0)
                        {

                            if (count == 1)
                            {
                                win = true;
                            }
                            Console.WriteLine($"У вас осталось {mistake} попыток!");
                            Console.Write("Ваше слово:");
                            for (int i = 0; i < letters.Length; i++)
                            {
                                Console.Write(t[i]);
                            }
                            Console.WriteLine();
                            Console.Write("Буквы, которые вы не угадали: ");
                            foreach (char c in mis)
                            {
                                Console.Write(c + " ");
                            }
                            Console.WriteLine();
                            Console.Write("Угадайте букву: ");
                            let = Convert.ToChar(Console.ReadLine());
                            int n = 0;
                            for (int i = 0; i <= letters.Length; i++)
                            {
                                if (i == letters.Length)
                                {
                                    if (n == 0)
                                    {
                                        Console.Clear();
                                        mistake--;
                                        mis.Add(let);
                                        break;
                                    }
                                    else if (n > 0)
                                    {
                                        Console.Clear();
                                        Console.WriteLine($"Вы молодец, угадали {n} букву(ы)!");
                                        break;
                                    }
                                }
                                else if (letters[i] == let)
                                {
                                    count--;
                                    t[i] = let;
                                    n++;
                                    Console.Clear();
                                }

                            }
                        }
                        if (mistake == 0)
                        {
                            Console.WriteLine("Вы не угадали слово!");
                        }
                        else if (win == true)
                        {
                            Console.Write("Вот загаданное слово: ");
                            for (int i = 0; i < letters.Length; i++)
                            {
                                Console.Write(t[i]);
                            }
                            Console.WriteLine();
                            Console.WriteLine("Поздравляю! Вы угадали слово!");
                        }

                        Console.WriteLine("<><><><><><><><><><><><><><><><><><><><>");
                        Console.Write("Желаете сыграть еще раз? (y/n)");
                        repeat = Convert.ToChar(Console.ReadLine());
                        if (repeat != 'y')
                        {
                            Console.WriteLine("Заходите поиграть еще!");
                        }
                        Console.Clear();
                    }
                }
                else
                {
                    Console.WriteLine("Где вы увидели такой вариант ответа?");
                }
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();


        }
    }
}

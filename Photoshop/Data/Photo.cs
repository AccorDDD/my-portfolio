using MyPhotoshop.Data;
using System;

namespace MyPhotoshop
{
    public class Photo
    {
        private readonly int width;
        private readonly int height;
        private readonly Pixel[,] data;
        public int Width => width;
        public int Height => height;
        public Pixel this[int W, int H]
        {
            get
            {
                return data[W, H];
            }
        }

        public Photo(int width, int height)
        {
            this.width = width;
            this.height = height;

            data = new Pixel[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    data[x, y] = new Pixel();
                }
            }
        }
    }
}


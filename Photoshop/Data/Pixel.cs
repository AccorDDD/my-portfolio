﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPhotoshop.Data
{
    public class Pixel
    {
        public static int ToChannel(double val)
        {
            return (int)(val * 255);
        }

        public static double ControlValues(double v)
        {
            if (v > 1) return 1;
            else if (v < 0) return 0;
            return v;
        }
        private double r;

        private double g;

        private double b;
        public double R
        {
            get
            {
                return r;
            }
            set
            {
                r = ForCheck(value);
            }
        }

        public double G
        {
            get
            {
                return g;
            }
            set
            {
                g = ForCheck(value);
            }
        }

        public double B
        {
            get
            {
                return b;
            }
            set
            {
                b = ForCheck(value);
            }
        }

        private double ForCheck(double value)
        {
            if (value > 1 || value < 0)
            {
                throw new PhotoshopException(value);
            }
            return value;
        }
    }

    class PhotoshopException : ArgumentException 
    {

        private readonly double value;

        public PhotoshopException(double Value)
        {
            value = Value;
        }

        public override string Message
        {
            get
            {
                return $"Wrong channel value {value} (the value must be between 0 and 1)" + base.Message;
            }
        }
    }
}
